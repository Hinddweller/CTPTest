#pragma once
#include "ThostFtdcTraderApi.h"
#include <stdlib.h>
#include <stdio.h>
#include <boost/algorithm/string.hpp>
using namespace std;
#include <string.h>

class CTraderSpi : public CThostFtdcTraderSpi
{
    public:
        CTraderSpi();
        ~CTraderSpi();
    public:
	    virtual void OnFrontConnected();
        virtual void OnFrontDisconnected(int nReason);
        virtual void OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
        //virtual void OnRspOrderInsert(CThostFtdcInputOrderField *pInputOrder, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
        virtual void OnRspOrderAction(CThostFtdcInputOrderActionField *pInputOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
        virtual void OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
        virtual void OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast);
        virtual void OnRtnOrder(CThostFtdcOrderField *pOrder);
        virtual void OnRtnTrade(CThostFtdcTradeField *pTrade);
    public:
        void InitUsrClient(string traderAddr, string brokerId, string usrId, string pwd, string investorId);
        void SetEnableFlag(bool flag)
        {
            m_bEnableFlag = flag;
        }
        void SetUserID(const string &usrID)
        {
            m_UsrID = usrID;
        }
        void SetBrokerID(const string &brokerID)
        {
            m_BrokerID = brokerID;
        }
        void SetPassword(const string &pwd)
        {
            m_Psw = pwd;
        }
        void SetInvestorID(const string &investorID)
        {
            m_InvestorID = investorID;
        }
        void SetTradeAddr(const string& tradeAddr)
        {
            m_TraderAddr = tradeAddr;
        }
        void SetTraderApi(CThostFtdcTraderApi* pTraderApi)
        {
            m_pTraderApi = pTraderApi;
        }

    private:
        int ReqUserLogin();
        int ReqSettlementInfoConfirm();
        int ReqOrderInsert();
        int ReqQryInvestorPosition(char* instrument);
	    bool IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo);
    private:
        string instrumentIDs[100];
        int m_nIntrCnt;
        CThostFtdcTraderApi *m_pTraderApi;
        string m_BrokerID;
        string m_Psw;
        string m_UsrID;
        string m_InvestorID;
        string m_TraderAddr;
        int m_SessionID;
        int m_FrontID;
        int m_nMsgSeqNum;
        bool m_bEnableFlag;
        bool m_ConnectFlag ;
        int m_nOrderRef;
        void* m_p_ctp_lib_handle;
};



