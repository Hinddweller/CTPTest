CXX = g++
CXXFLAGS = -g #-pthread

#LDFLAGS = -L. #-ldl
#LIBS = -lthosttraderapi -lboost_system #-lboost_chrono

LDFLAGS = -L. 
LIBS = -ldl -lboost_system -lboost_chrono -lboost_date_time -lboost_thread

EXE = test.out
OBJ = TraderSpi.o main.o

$(EXE) : $(OBJ)
	$(CXX) $(CXXFLAGS) $(OBJ) -o $(EXE) $(LDFLAGS) $(LIBS)

main.o: main.cpp TraderSpi.h 
	$(CXX) $(CXXFLAGS) -c main.cpp

TraderSpi.o:  TraderSpi.h TraderSpi.cpp
	$(CXX) $(CXXFLAGS) -c TraderSpi.cpp

clean:
	rm -f $(OBJ)
	
