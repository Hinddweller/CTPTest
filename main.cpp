
#include <iostream>
#include <dlfcn.h>
#include <string>
#include "TraderSpi.h"
#include <map>
#include <utility>

using namespace std;


CThostFtdcTraderApi* pUserApi;

/*
bool load_config(map<string, string> & configMap)
{

	//string config = "config-citicf.txt";
	string config = "config.txt";
	ifstream is_file(config.c_str());
	
	string line;
	while( std::getline(is_file, line) )

	{
		istringstream  is_line(line);
		string key = "";
		if( getline(is_line, key, '=') )
		{
			string value = "";
			if( getline(is_line, value) )
			{
				configMap.insert(std::pair<string,string>(key, value));
			}
		}
	}
	return true;
	
}
*/

string trader_front_addr = "tcp://180.168.146.187:10000";
string trader_broker_id = "9999";
string trader_user_id = "116124";
string trader_user_password = "cafg2017";
string investor_id = "116124";


int main()
{
    /*
    map<string, string> configMap;
    load_config(configMap);

	string trader_front_addr =  configMap["trader_front_addr"];
	string trader_broker_id =  configMap["trader_broker_id"];
	string trader_user_id =  configMap["trader_user_id"];
	string trader_user_password =  configMap["trader_user_password"];
	string investor_id =  configMap["investor_id"];
    */

    void *handle;
    handle = dlopen("./thosttraderapi.so", RTLD_NOW);
    if (handle == NULL)
    {
        cout << "CTP thosttraderapi.so not loaded." << endl;
    }
    else
    {
        cout << "CTP thosttraderapi.so loaded." << endl;
    }


    typedef CThostFtdcTraderApi* (*CreateFtdcTradeApiPtr)(char const*);
    CreateFtdcTradeApiPtr CreateTradeApi = (CreateFtdcTradeApiPtr)dlsym(handle, "_ZN19CThostFtdcTraderApi19CreateFtdcTraderApiEPKc");

    char traderFlowPath[128];
    strcpy(traderFlowPath, "./tradeflow");
    //pUserApi = CThostFtdcTraderApi::CreateFtdcTraderApi(traderFlowPath);
    pUserApi = CreateTradeApi(traderFlowPath);
    CTraderSpi userSpi;
    userSpi.InitUsrClient(trader_front_addr, trader_broker_id, trader_user_id, trader_user_password, investor_id);
    userSpi.SetTraderApi(pUserApi);
    pUserApi->RegisterSpi(&userSpi);
    pUserApi->SubscribePublicTopic(THOST_TERT_QUICK);
    pUserApi->SubscribePrivateTopic(THOST_TERT_QUICK);
    char frontAdd[128];
    strcpy(frontAdd, trader_front_addr.c_str());
    pUserApi->RegisterFront(frontAdd);
    pUserApi->Init();
    pUserApi->Join();

    return 0;
}
