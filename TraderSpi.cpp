#include "TraderSpi.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <boost/thread/thread.hpp>

CTraderSpi::CTraderSpi(void)
{
	memset (&instrumentIDs, 0, sizeof(instrumentIDs));
	int m_nIntrCnt = 0;
	m_pTraderApi = NULL;
	//m_fillLastestId = "";
	m_nMsgSeqNum = 0;
	m_nOrderRef = 0;
	m_ConnectFlag = false;
	SetEnableFlag(false);
}

CTraderSpi::~CTraderSpi(void) {}

void CTraderSpi::InitUsrClient(string traderAddr, string brokerId, string usrId, string pwd, string investorId)
{ 
	SetUserID(usrId);
	SetPassword(pwd);
	SetBrokerID(brokerId);
	SetTradeAddr(traderAddr);
	SetInvestorID(investorId);
}


void CTraderSpi::OnFrontConnected()
{
	m_ConnectFlag = true;
	string strTip = "Trader---->>> OnFrontConnected: Trading Server Connected";
	cout << strTip << endl;
	ReqUserLogin();
}

int CTraderSpi::ReqUserLogin()
{
	CThostFtdcReqUserLoginField req;
//	string ProductInfo = "HxqhJGB";
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, m_BrokerID.c_str());
	strcpy(req.UserID, m_UsrID.c_str());
	strcpy(req.Password, m_Psw.c_str());

	//strcpy(req.InterfaceProductInfo,ProductInfo.c_str());
	//strcpy(req.UserProductInfo,ProductInfo.c_str());
	int iResult = m_pTraderApi->ReqUserLogin(&req, ++m_nMsgSeqNum);
	cout << "Trader---->>> ReqUserLogin => Request user login: " << ((iResult == 0) ? "Success" : "Fail") << endl;
	return iResult;
}

void CTraderSpi::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,
	CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "Trader---->>> " << "OnRspUserLogin" << endl;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		char strMsg[128] = "";
		m_nOrderRef = atoi(pRspUserLogin->MaxOrderRef) + 10000;
        m_SessionID = pRspUserLogin->SessionID;
        m_FrontID = pRspUserLogin->FrontID;
		cout << "TradingDay: " << pRspUserLogin->TradingDay
			<< " FrontID: " << pRspUserLogin->FrontID
			<< " SessionID: " << pRspUserLogin->SessionID
			<< " MaxOrderRef: " << pRspUserLogin->MaxOrderRef
			<< " m_nOrderRef: " << m_nOrderRef
			<<endl;
		// CTP requires check settlement before trading, added by Jie
		ReqSettlementInfoConfirm();

	}else {
		string errorMsg = pRspInfo->ErrorMsg;
		cout << "User login failed: "
			<< errorMsg
			<< endl;
	}
}

int CTraderSpi::ReqSettlementInfoConfirm()
{
	CThostFtdcSettlementInfoConfirmField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, m_BrokerID.c_str());
	strcpy(req.InvestorID, m_InvestorID.c_str());

	int iResult = m_pTraderApi->ReqSettlementInfoConfirm(&req, ++m_nMsgSeqNum);
	cout << "Trader---->>> ReqSettlementInfoConfirm: " 
		 << ((iResult == 0) ? "Success" : "Fail") << endl;
	return iResult;
}

void CTraderSpi::OnRspSettlementInfoConfirm(CThostFtdcSettlementInfoConfirmField *pSettlementInfoConfirm, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "Trader---->>> " << __FUNCTION__ << endl;
	if (pRspInfo->ErrorID == 0)
	{
		cout << "SettlementInfoConfirm Success" 
			 << ", "
			 << pSettlementInfoConfirm->ConfirmDate
			 << ", "
			 << pSettlementInfoConfirm->ConfirmTime << endl;
        //
        char instID[31];
        strcpy(instID, "ag1806");
        ReqQryInvestorPosition(instID);
	}
	else
	{
		string errMsg = pRspInfo->ErrorMsg;
		cout << "SettlementInfoConfirm failed: "
			 << errMsg << endl;
	}
}

int CTraderSpi::ReqOrderInsert()
{
    char inst[128];
    strcpy(inst, "ag1806");

    CThostFtdcInputOrderField req;
    memset(&req, 0, sizeof(req));
    strcpy(req.BrokerID, m_BrokerID.c_str());
    strcpy(req.InvestorID, m_InvestorID.c_str());
    strcpy(req.InstrumentID, inst);
    strcpy(req.UserID, m_UsrID.c_str());
    ++m_nOrderRef;
    sprintf(req.OrderRef, "%d", m_nOrderRef);
    req.Direction = THOST_FTDC_D_Buy;
    req.CombOffsetFlag[0] = THOST_FTDC_OF_Open;
    req.CombHedgeFlag[0] = THOST_FTDC_HF_Speculation;
    req.VolumeTotalOriginal = 1;
    req.ContingentCondition = THOST_FTDC_CC_Immediately;
    req.VolumeCondition = THOST_FTDC_VC_AV;
    req.MinVolume = 1;
    req.ForceCloseReason = THOST_FTDC_FCC_NotForceClose;
    req.IsAutoSuspend = 0;
    req.UserForceClose = 0;
    req.OrderPriceType = THOST_FTDC_OPT_LimitPrice;
    req.LimitPrice = 3700;
    req.TimeCondition = THOST_FTDC_TC_GFD;
    //PlaceOrder
    int iResult = m_pTraderApi->ReqOrderInsert(&req, ++m_nMsgSeqNum);
    char buf[1024];
    sprintf(buf, "Trader---->>> Called ReqOrderInsert() with Ref %d %s", m_nOrderRef, ((iResult==0)?"OK":"Failed"));
    cout << buf << endl;
}

void CTraderSpi::OnRtnOrder(CThostFtdcOrderField *pOrder)
{
	int nOrderRef = atoi(pOrder->OrderRef);
	//DEBUG by Jie
    char buf[512];
    sprintf(buf, ", OrderStatus %c, %d", pOrder->OrderStatus, pOrder->OrderStatus);
	cout << "Trader---->>> " << __FUNCTION__
	     << " with OrderRef: " << pOrder->OrderRef << buf << endl;

    if (pOrder->OrderStatus == THOST_FTDC_OST_NoTradeQueueing)
    {
        cout << "Trader---->>> Order with OrderRef: " << pOrder->OrderRef << " queueing." << endl;  
        boost::this_thread::sleep(boost::posix_time::milliseconds(5000));
        cout << "Trader---->>> Waited 5 seconds. Cancel order." << endl;

        CThostFtdcInputOrderActionField req;
        memset(&req, 0, sizeof(req));
        strcpy(req.BrokerID, m_BrokerID.c_str());
        strcpy(req.InvestorID, m_InvestorID.c_str());
        strcpy(req.UserID, m_UsrID.c_str());

        req.FrontID = m_FrontID;
        req.SessionID = m_SessionID;
        strcpy(req.OrderRef, pOrder->OrderRef);
        req.ActionFlag = THOST_FTDC_AF_Delete;

        int iResult = m_pTraderApi->ReqOrderAction(&req, ++m_nMsgSeqNum);
        cout << "Trader---->>> Called ReqOrderAction() with OrderRef: " << pOrder->OrderRef 
             << ", " << ((iResult==0) ? "OK": "Fail") << endl;;
    }

    if (pOrder->OrderStatus == THOST_FTDC_OST_Canceled)
    {
        cout << "Trader---->>> Canelled order with OrderRef:" << pOrder->OrderRef << endl;;
    }

	return;
}

void CTraderSpi::OnRtnTrade(CThostFtdcTradeField *pTrade)
{
	int nOrderRef = atoi(pTrade->OrderRef);
	cout << "Trader---->>> " << __FUNCTION__
	     << " with OrderRef: " << pTrade->OrderRef << endl;

    return;
}

int CTraderSpi::ReqQryInvestorPosition(char* instrument)
{

	CThostFtdcQryInvestorPositionField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, m_BrokerID.c_str());
	strcpy(req.InvestorID, m_InvestorID.c_str());
	strcpy(req.InstrumentID, instrument);

	int iResult = m_pTraderApi->ReqQryInvestorPosition(&req, ++m_nMsgSeqNum);
	cout << "Trader---->>> " << __FUNCTION__ 
		 << " " << instrument
	     << " " << ((iResult == 0) ? "Success" : "Fail") << endl;

	return iResult;
}

void CTraderSpi::OnRspQryInvestorPosition(CThostFtdcInvestorPositionField *pInvestorPosition, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	cout << "Trader---->>> " << __FUNCTION__ << endl;
	
	if ( !IsErrorRspInfo(pRspInfo) )
	{
        if (!pInvestorPosition->InstrumentID)
        {
	        cout << "No position info for the required instrument." << endl;
        }
        else
        {
            char buf[512];
            sprintf(buf, "InstID: %s, Date: %s, PosDir: %c, TdyPos:%d, YdPos:%d, Pos:%d", 
                    pInvestorPosition->InstrumentID,
                    pInvestorPosition->TradingDay,
                    pInvestorPosition->PosiDirection,
                    pInvestorPosition->TodayPosition,
                    pInvestorPosition->YdPosition,
                    pInvestorPosition->Position
                    );
            cout << buf << endl;
        }
        if (bIsLast)
        {
            ReqOrderInsert();
        }
	}
	else
	{
		cout << pInvestorPosition->InstrumentID
			<< " Error, ErrorID: " << pRspInfo->ErrorID << ", " << pRspInfo->ErrorMsg;
	}
	return;
}

void CTraderSpi::OnRspOrderAction(CThostFtdcInputOrderActionField *pInputOrderAction, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	char buf[256];
	int nOrderRef = atoi(pInputOrderAction->OrderRef);
	cout << "Trader---->>> " << __FUNCTION__
	     << " with OrderRef: " << pInputOrderAction->OrderRef << endl;
	if (pRspInfo->ErrorID != 0)
	{
		sprintf(buf, "OnRspOrderAction: %d with error code %d: %s", nOrderRef, pRspInfo->ErrorID, pRspInfo->ErrorMsg);
        cout << buf << endl;
	}
}

bool CTraderSpi::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo)
{
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		cout << "Trader---->>> ErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << endl;
	}
	return bResult;
}

void CTraderSpi::OnFrontDisconnected(int nReason)
{
	cout << "Trader---->>> OnFrontDisconnted" << endl;
	return; 
}

